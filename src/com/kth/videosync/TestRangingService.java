package com.kth.videosync;

import java.util.Collection;

import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.RangeNotifier;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.app.Service;
import android.content.Intent;

public class TestRangingService extends Service implements IBeaconConsumer {
	protected final String TAG = "RangingService";
	private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);
	
	//private Handler handler;
	
	public TestRangingService() {
		super();
		//this.handler = MainActivity.ibeacon_handler;
		Log.v(TAG, "rangingservice Constructor---");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		iBeaconManager.bind(this);
		Log.v(TAG, "ranging service onCreate started---");
	}

	@Override
	public void onIBeaconServiceConnect() {
		Log.v(TAG, "@~~~~~~~~~~~~~~@ ");
		iBeaconManager.setRangeNotifier(new RangeNotifier() {
			@Override
			public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {

				Log.v(TAG, "@@@@@@@@@@@@@@@ ");
			}
		});

		try {
			iBeaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
			} 
		catch (RemoteException e) {	}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.v(TAG, "rangingservice onStartCommand---");
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		iBeaconManager.unBind(this);
		Log.v(TAG, "ranging service onDestroy :(((((");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}
