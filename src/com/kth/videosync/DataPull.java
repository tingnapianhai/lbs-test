package com.kth.videosync;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DataPull extends AsyncTask<String, Void, JSONArray> {

	private static final String TAG = "test";
	private static String MY_API_KEY = "AIzaSyC9vEUBvhI0ZBWWi81TyvgMgzH0x5c1Zxk";
	public static String PLACES_API_KEY = "&key=" + MY_API_KEY;
	public static String ENIRO_URI = "http://api.eniro.com/cs/proximity/basic?country=se&version=1.1.3&profile=pavan4&key=8041689710622693062&max_distance=50";
	public static String GOOGLE_URI = "https://maps.googleapis.com/maps/api/place/search/xml?&radius=50&sensor=true";

	public static String ADDRESS_URI = "https://maps.googleapis.com/maps/api/geocode/xml?sensor=true";
	static String name = "gsdfgsfdg";
	Context context;
	JSONArray companies;
	LinearLayout layout;
	private int interest_radius = 30;
	JSONObject location_eniro;
	float result[] = new float[4];

	public DataPull(Context msgHand, LinearLayout ll) {
		super();
		context = msgHand;
		layout = ll;
		layout.removeAllViews();
	}

	// @Override
	// protected void onHandleIntent(Intent arg0) {
	// lat = (String) arg0.getExtras().get("latitude");
	// longi = (String) arg0.getExtras().get("longitude");
	//
	// getEniroData(lat, longi);
	// }

	public JSONArray getEniroData(String lat, String longi) {
		companies = new JSONArray();
		String resp;
		try {
			URL obj = new URL(ENIRO_URI + "&latitude=" + lat + "&longitude="
					+ longi);
			Log.v("test", ENIRO_URI + "&latitude=" + lat + "&longitude="
					+ longi);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			// int responseCode = con.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream(), HTTP.UTF_8));
			String inputLine;
			StringBuffer response = new StringBuffer();

			StringBuilder builder = new StringBuilder();
			while ((inputLine = in.readLine()) != null) {
				// response.append(inputLine);
				builder.append(inputLine);
			}
			in.close();

			JSONTokener tokener = new JSONTokener(builder.toString());

			resp = response.toString();
			JSONObject object = new JSONObject(tokener);
			JSONArray companies_temp = object.getJSONArray("adverts");

			for (int i = 0; i < companies_temp.length(); i++) {
				String street_no = ((JSONObject) ((JSONObject) companies_temp
						.get(i)).get("address")).get("streetName").toString();
				street_no = street_no.replaceAll("\\s+", "");
				location_eniro = (((JSONArray) ((JSONObject) ((JSONObject) companies_temp
						.get(i)).get("location")).get("coordinates"))
						.getJSONObject(0));
				Location.distanceBetween(Double.parseDouble(lat), Double
						.parseDouble(longi), Double.parseDouble(location_eniro
						.getString("latitude")), Double
						.parseDouble(location_eniro.getString("longitude")),
						result);
				Log.v("test",
						i
								+ "found"
								+ result[0]
								+ ((JSONObject) ((JSONObject) companies_temp
										.get(i)).get("companyInfo")).get(
										"companyName").toString() + "address "
								+ street_no);

				if (result[0] < interest_radius
						&& street_no.contains(AddressPull.street_num)) {//

					String name = ((JSONObject) ((JSONObject) companies_temp
							.get(i)).get("companyInfo")).get("companyName")
							.toString();
					((JSONObject) ((JSONObject) companies_temp.get(i))
							.get("companyInfo")).remove("companyName");
					((JSONObject) ((JSONObject) companies_temp.get(i))
							.get("companyInfo")).put("companyName", name);
//							+ "    " + result[0]);
					companies.put(companies_temp.get(i));
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			resp = null;
		}
		return companies;
	}

	private JSONArray refreshPlaces(String lat, String longi, int radius)
			throws JSONException {
		// Log to see if we'll be prefetching the details page of each new
		// place.
		long currentTime = System.currentTimeMillis();
		URL url;
		companies = new JSONArray();
		try {
			// TODO Replace this with a URI to your own service.
			String locationStr = lat + "," + longi;
			String baseURI = GOOGLE_URI;
			String placesFeed = baseURI + "&location=" + locationStr
					+ "&radius=" + radius + PLACES_API_KEY;
			url = new URL(placesFeed);

			// Open the connection
			URLConnection connection = url.openConnection();
			HttpsURLConnection httpConnection = (HttpsURLConnection) connection;
			int responseCode = httpConnection.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				// Use the XML Pull Parser to extract each nearby location.
				// TODO Replace the XML parsing to extract your own place list.
				InputStream in = httpConnection.getInputStream();
				XmlPullParserFactory factory = XmlPullParserFactory
						.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();

				xpp.setInput(in, null);
				int eventType = xpp.getEventType();
				while (eventType != XmlPullParser.END_DOCUMENT) {
					if (eventType == XmlPullParser.START_TAG
							&& xpp.getName().equals("result")) {
						eventType = xpp.next();
						String id = "";
						String name = "";
						String vicinity = "";
						String types = "";
						String locationLat = "";
						String locationLng = "";
						String viewport = "";
						String icon = "";
						String reference = "";
						while (!(eventType == XmlPullParser.END_TAG && xpp
								.getName().equals("result"))) {
							if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("name"))
								name = xpp.nextText();
							else if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("vicinity"))
								vicinity = xpp.nextText();
							else if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("type"))
								types = types == "" ? xpp.nextText() : types
										+ " " + xpp.nextText();
							else if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("lat"))
								locationLat = xpp.nextText();
							else if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("lng"))
								locationLng = xpp.nextText();
							else if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("icon"))
								icon = xpp.nextText();
							else if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("reference"))
								reference = xpp.nextText();
							else if (eventType == XmlPullParser.START_TAG
									&& xpp.getName().equals("id"))
								id = xpp.nextText();
							eventType = xpp.next();
						}

						Log.v(TAG, name);

						// ((JSONObject)((JSONObject)
						// companies.get(i)).get("companyInfo")).get("companyName");
						JSONObject obj = new JSONObject();
						JSONObject obj1 = new JSONObject();

						Location.distanceBetween(Double.parseDouble(lat),
								Double.parseDouble(longi),
								Double.parseDouble(locationLat),
								Double.parseDouble(locationLng), result);
						if (result[0] < interest_radius) {
							obj.put("companyInfo",
									obj1.put("companyName", name));
//							+ "    "+ result[0]));
							companies.put(obj);
						}
						// Add each new place to the Places Content Provider
					}
					eventType = xpp.next();
				}

			} else
				Log.e(TAG,
						responseCode + ": "
								+ httpConnection.getResponseMessage());
		} catch (MalformedURLException e) {
			Log.e(TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		} catch (XmlPullParserException e) {
			Log.e(TAG, e.getMessage());
		} finally {
		}
		return companies;
	}

	protected void onPostExecute(JSONArray result) {
		Log.v("test", "setting text");
		if (result != null) {
			for (int i = 0; i < result.length(); i++) {
				TextView tv1 = new TextView(this.context);
				try {
					tv1.setText(((JSONObject) ((JSONObject) result.get(i))
							.get("companyInfo")).get("companyName").toString());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				layout.addView(tv1);
			}
		}

	}

	@Override
	protected JSONArray doInBackground(String... arg0) {

		if (FusedMain.google) {
			try {
				return refreshPlaces(arg0[0], arg0[1], 50);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return companies;
		} else
			return getEniroData(arg0[0], arg0[1]);

	}

}
