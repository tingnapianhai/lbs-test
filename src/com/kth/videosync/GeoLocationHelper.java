package com.kth.videosync;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;

public class GeoLocationHelper {

	private static final int REFERENCE_TIME_DELTA = 2 * 60 * 1000;

	private static final int REFERENCE_ACCURACY_DELTA = 200;

	public static boolean isGpsProviderEnabled(Context context) {
		LocationManager service = (LocationManager) context
				.getSystemService(android.content.Context.LOCATION_SERVICE);
		return service.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public static boolean isNetworkProviderEnabled(Context context) {
		LocationManager service = (LocationManager) context
				.getSystemService(android.content.Context.LOCATION_SERVICE);
		return service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}

	public static String getLocationProviderByPriority(Context context) {
		if (isGpsProviderEnabled(context)) {
			return LocationManager.GPS_PROVIDER;
		} else if (isNetworkProviderEnabled(context)) {
			return LocationManager.NETWORK_PROVIDER;
		} else {
			return null;
		}
	}

	public static void showGpsSettingsScreen(Activity parentActivity) {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		parentActivity.startActivity(intent);
	}

	public static boolean isSameProvider(String provider1, String provider2) {
		if (provider1 == null) {
			return provider2 == null;
		}
		return provider1.equals(provider2);
	}

	public static boolean isBetterLocation(Location location,
			Location currentBestLocation) {
		if (currentBestLocation == null) {
			return true;
		}

		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > REFERENCE_TIME_DELTA;
		boolean isSignificantlyOlder = timeDelta < -REFERENCE_TIME_DELTA;
		boolean isNewer = timeDelta > 0;

		// If it's been more than REFERENCE_TIME_DELTA since the current
		// location, use
		// the new location because the user has likely moved
		if (isSignificantlyNewer) {
			return true;
		} else if (isSignificantlyOlder) {
			// If the new location is more than REFERENCE_TIME_DELTA older, it
			// must be
			// worse
			return false;
		}

		// Check whether the new location fix is more or less accurate
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
				.getAccuracy());
		boolean isLessAccurate = accuracyDelta > 0;
		boolean isMoreAccurate = accuracyDelta < 0;
		boolean isSignificantlyLessAccurate = accuracyDelta > REFERENCE_ACCURACY_DELTA;

		// Check if the old and new location are from the same provider
		boolean isFromSameProvider = isSameProvider(location.getProvider(),
				currentBestLocation.getProvider());

		// Determine location quality using a combination of timeliness and
		// accuracy
		if (isMoreAccurate) {
			return true;
		} else if (isNewer && !isLessAccurate) {
			return true;
		} else if (isNewer && !isSignificantlyLessAccurate
				&& isFromSameProvider) {
			return true;
		}
		return false;
	}
}