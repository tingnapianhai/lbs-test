/************************************************************************************
 * Copyright (c) 2012 Paul Lawitzki
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ************************************************************************************/

package com.kth.videosync;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class SensorFusionActivity implements SensorEventListener {

	private static SensorManager mSensorManager = null;
	private float[] gyro = new float[3];
	private float[] gyroMatrix = new float[9];
	private float[] gyroOrientation = new float[3];
	int angle_x = 0;
	int angle_y = 0;
	private float[] magnet = new float[3];
	private float[] accel = new float[3];
	private float[] accMagOrientation = new float[3];
	private float[] fusedOrientation = new float[3];
	private float[] rotationMatrix = new float[9];
	private float[] result_1 = new float[5];

	public static final float EPSILON = 0.000000001f;
	private static final float NS2S = 1.0f / 1000000000.0f;
	private float timestamp;
	private boolean initState = true;
	private static float angle[] = { 0.0f, 0.0f, 0.0f };
	private static float angle_pointer[] = { 0.0f, 0.0f, 0.0f };
	public static final int TIME_CONSTANT = 30;
	public static final float FILTER_COEFFICIENT = 0.7f;
	// private Timer fuseTimer = new Timer();
	private static float prev_angle = 0;
	private static float y_pointer = 0;
	calculateFusedOrientationTask task;
	int count = 0;

	// The following members are only for displaying the sensor output.
	private Handler mHandler;
	DecimalFormat d = new DecimalFormat("#.##");

	public SensorFusionActivity(SensorManager sm, Handler mapHandler) {
		mHandler = mapHandler;
		gyroOrientation[0] = 0.0f;
		gyroOrientation[1] = 0.0f;
		gyroOrientation[2] = 0.0f;

		// initialise gyroMatrix with identity matrix
		gyroMatrix[0] = 1.0f;
		gyroMatrix[1] = 0.0f;
		gyroMatrix[2] = 0.0f;
		gyroMatrix[3] = 0.0f;
		gyroMatrix[4] = 1.0f;
		gyroMatrix[5] = 0.0f;
		gyroMatrix[6] = 0.0f;
		gyroMatrix[7] = 0.0f;
		gyroMatrix[8] = 1.0f;

		// get sensorManager and initialise sensor listeners
		mSensorManager = sm;
		task = new calculateFusedOrientationTask();
		initListeners();

		// mHandler = new Handler();
		d.setRoundingMode(RoundingMode.HALF_UP);
		d.setMaximumFractionDigits(3);
		d.setMinimumFractionDigits(3);
	}

	public void Stop() {
		mSensorManager.unregisterListener(this);
	}

	// This function registers sensor listeners for the accelerometer,
	// magnetometer and gyroscope.
	public void initListeners() {
		mSensorManager.registerListener(this,
				mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
				SensorManager.SENSOR_DELAY_GAME);
		// mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),100000);
		// mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),
		// SensorManager.SENSOR_DELAY_GAME);
		// mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),100000);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			// copy new accelerometer data into accel array and calculate
			// orientation
			System.arraycopy(event.values, 0, accel, 0, 3);
			calculateAccMagOrientation();

			break;

		case Sensor.TYPE_GYROSCOPE:
			// process gyro data
			// gyroFunction(event);
			// task.run();

			// float dT = 0;
			// if (timestamp != 0) {
			// dT = (event.timestamp - timestamp) * NS2S;
			// angle_x = (float) (event.values[2] * 100);// take two digits
			// after
			// angle_y = (int) (event.values[0] * 100);
			// angle[2] = angle[2] + angle_x * dT;
			// angle[0] = angle[0] + angle_y * dT;
			// angle_pointer[0] = 0.3f * 26 * angle[0] + 0.7f *
			// angle_pointer[0];// 26 is the coefficient to covert to
			// // coordinate
			// angle_pointer[2] = 0.3f * 26 * angle[2] + 0.7f*
			// angle_pointer[2];// 26 is the coefficient to covert to
			// // coordinate
			// }
			// timestamp = event.timestamp;
			// break;

		case Sensor.TYPE_MAGNETIC_FIELD:
			// copy new magnetometer data into magnet array
			System.arraycopy(event.values, 0, magnet, 0, 3);
			break;

		case Sensor.TYPE_ROTATION_VECTOR:
			angleFromRotation(event);
			break;
		}

	}

	private void angleFromRotation(SensorEvent se) {
		double m = 0;
		double z = Math.toDegrees(2 * Math.asin(se.values[2]));

		// count = count+1;
		if (prev_angle * z < 0 && z > 90) {
			if (z > 0) {
				z = .85 * (360 + prev_angle) + .15 * z;
				if (z > 180)
					z = z - 360;
			} else {
				z = 0.85 * (prev_angle - 360) + .15 * z;
				if (z < -180)
					z = z + 360;
			}
		}
		// else{
		// float coeff = (float) ((Math.abs(z) -177)/(180-177));
		// if(z>177 && z<=180){
		// z =(.85*coeff)*(prev_angle) + (1-0.85*coeff)*z;
		// }else if(z<=-180 && z>-177){
		// z = 0.85*coeff*(prev_angle) + (1-0.85*coeff)*z;
		// }
		// }
		m = z;

		float avg = (float) calibrateCompassValue(z);
		// Log.v("test", avg+ "  avg " );

		// avg = (float) (prev_angle * 0.8 + avg * 0.2);

		// if (count > 1){
		// if(Math.abs(result_1[count - 2] - result_1[count-1])>300){
		// result_1 = new float[10];
		// count= 2;
		// result_1[count-1] = avg;
		// result_1[count-2] = avg;
		// }
		// else result_1[count - 1] = (float) (avg * 0.1 + result_1[count - 2] *
		// 0.9);
		// }
		// else
		// result_1[count - 1] = avg;
		//
		// float sum = 0;
		// float avg1 = 0;
		//
		// for (int i = 0; i < count; i++) {
		// sum = (float) (sum + result_1[i]);
		// avg1 = (float) sum / count;
		// }
		// avg = (float) (avg1 * 0.2 + avg * 0.8);
		FusedMain.direction_value = avg;

		Message msg2 = mHandler.obtainMessage();
		msg2.obj = avg;
		msg2.what = 1;
		mHandler.sendMessage(msg2);
		// count = count % 5;

		prev_angle = (float) m;
	}

	public double calibrateCompassValue(double e) {
		double m = 0;
		if (e < 0)
			m = e * -1;
		else
			m = 360 - e;
		// m = (360 - e) % 360;
		// if(e <0) e = e+360 ;
		// Log.v("test",""+m+"m " + e+ " e " );
		return m;
	}

	// calculates orientation angles from accelerometer and magnetometer output
	public void calculateAccMagOrientation() {
		if (SensorManager
				.getRotationMatrix(rotationMatrix, null, accel, magnet)) {
			SensorManager.getOrientation(rotationMatrix, accMagOrientation);
		}
	}

	// This function is borrowed from the Android reference
	// at
	// http://developer.android.com/reference/android/hardware/SensorEvent.html#values
	// It calculates a rotation vector from the gyroscope angular speed values.
	private void getRotationVectorFromGyro(float[] gyroValues,
			float[] deltaRotationVector, float timeFactor) {
		float[] normValues = new float[3];

		// Calculate the angular speed of the sample
		float omegaMagnitude = (float) Math
				.sqrt(gyroValues[0] * gyroValues[0] + gyroValues[1]
						* gyroValues[1] + gyroValues[2] * gyroValues[2]);

		// Normalize the rotation vector if it's big enough to get the axis
		if (omegaMagnitude > EPSILON) {
			normValues[0] = gyroValues[0] / omegaMagnitude;
			normValues[1] = gyroValues[1] / omegaMagnitude;
			normValues[2] = gyroValues[2] / omegaMagnitude;
		}

		// Integrate around this axis with the angular speed by the timestep
		// in order to get a delta rotation from this sample over the timestep
		// We will convert this axis-angle representation of the delta rotation
		// into a quaternion before turning it into the rotation matrix.
		float thetaOverTwo = omegaMagnitude * timeFactor;
		float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
		deltaRotationVector[0] = sinThetaOverTwo * normValues[0];
		deltaRotationVector[1] = sinThetaOverTwo * normValues[1];
		deltaRotationVector[2] = sinThetaOverTwo * normValues[2];
		deltaRotationVector[3] = cosThetaOverTwo;
	}

	// This function performs the integration of the gyroscope data.
	// It writes the gyroscope based orientation into gyroOrientation.
	public void gyroFunction(SensorEvent event) {

		// don't start until first accelerometer/magnetometer orientation has
		// been acquired
		if (accMagOrientation == null)
			return;

		// initialisation of the gyroscope based rotation matrix
		if (initState) {
			float[] initMatrix = new float[9];
			initMatrix = getRotationMatrixFromOrientation(accMagOrientation);
			float[] test = new float[3];
			SensorManager.getOrientation(initMatrix, test);
			gyroMatrix = matrixMultiplication(gyroMatrix, initMatrix);
			initState = false;
		}

		// copy the new gyro values into the gyro array
		// convert the raw gyro data into a rotation vector
		float[] deltaVector = new float[4];
		if (timestamp != 0) {
			final float dT = (event.timestamp - timestamp) * NS2S;
			System.arraycopy(event.values, 0, gyro, 0, 3);
			getRotationVectorFromGyro(gyro, deltaVector, dT / 2.0f);
		}

		// measurement done, save current time for next interval
		timestamp = event.timestamp;

		// convert rotation vector into rotation matrix
		float[] deltaMatrix = new float[9];
		SensorManager.getRotationMatrixFromVector(deltaMatrix, deltaVector);

		// apply the new rotation interval on the gyroscope based rotation
		// matrix
		gyroMatrix = matrixMultiplication(gyroMatrix, deltaMatrix);

		// get the gyroscope based orientation from the rotation matrix
		SensorManager.getOrientation(gyroMatrix, gyroOrientation);
	}

	private float[] getRotationMatrixFromOrientation(float[] o) {
		float[] xM = new float[9];
		float[] yM = new float[9];
		float[] zM = new float[9];

		float sinX = (float) Math.sin(o[1]);
		float cosX = (float) Math.cos(o[1]);
		float sinY = (float) Math.sin(o[2]);
		float cosY = (float) Math.cos(o[2]);
		float sinZ = (float) Math.sin(o[0]);
		float cosZ = (float) Math.cos(o[0]);

		// rotation about x-axis (pitch)
		xM[0] = 1.0f;
		xM[1] = 0.0f;
		xM[2] = 0.0f;
		xM[3] = 0.0f;
		xM[4] = cosX;
		xM[5] = sinX;
		xM[6] = 0.0f;
		xM[7] = -sinX;
		xM[8] = cosX;

		// rotation about y-axis (roll)
		yM[0] = cosY;
		yM[1] = 0.0f;
		yM[2] = sinY;
		yM[3] = 0.0f;
		yM[4] = 1.0f;
		yM[5] = 0.0f;
		yM[6] = -sinY;
		yM[7] = 0.0f;
		yM[8] = cosY;

		// rotation about z-axis (azimuth)
		zM[0] = cosZ;
		zM[1] = sinZ;
		zM[2] = 0.0f;
		zM[3] = -sinZ;
		zM[4] = cosZ;
		zM[5] = 0.0f;
		zM[6] = 0.0f;
		zM[7] = 0.0f;
		zM[8] = 1.0f;

		// rotation order is y, x, z (roll, pitch, azimuth)
		float[] resultMatrix = matrixMultiplication(xM, yM);
		resultMatrix = matrixMultiplication(zM, resultMatrix);
		return resultMatrix;
	}

	private float[] matrixMultiplication(float[] A, float[] B) {
		float[] result = new float[9];

		result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
		result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
		result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];

		result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
		result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
		result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];

		result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
		result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
		result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];

		return result;
	}

	class calculateFusedOrientationTask {
		// extends TimerTask {
		public void run() {
			count = count + 1;
			float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;

			/*
			 * Fix for 179� <--> -179� transition problem: Check whether one of
			 * the two orientation angles (gyro or accMag) is negative while the
			 * other one is positive. If so, add 360� (2 * math.PI) to the
			 * negative value, perform the sensor fusion, and remove the 360�
			 * from the result if it is greater than 180�. This stabilizes the
			 * output in positive-to-negative-transition cases.
			 */

			// azimuth
			if (gyroOrientation[0] < -0.5 * Math.PI
					&& accMagOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTER_COEFFICIENT
						* (gyroOrientation[0] + 2.0 * Math.PI) + oneMinusCoeff
						* accMagOrientation[0]);
				fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else if (accMagOrientation[0] < -0.5 * Math.PI
					&& gyroOrientation[0] > 0.0) {
				fusedOrientation[0] = (float) (FILTER_COEFFICIENT
						* gyroOrientation[0] + oneMinusCoeff
						* (accMagOrientation[0] + 2.0 * Math.PI));
				fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else {
				fusedOrientation[0] = FILTER_COEFFICIENT * gyroOrientation[0]
						+ oneMinusCoeff * accMagOrientation[0];
			}

			// pitch
			if (gyroOrientation[1] < -0.5 * Math.PI
					&& accMagOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTER_COEFFICIENT
						* (gyroOrientation[1] + 2.0 * Math.PI) + oneMinusCoeff
						* accMagOrientation[1]);
				fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else if (accMagOrientation[1] < -0.5 * Math.PI
					&& gyroOrientation[1] > 0.0) {
				fusedOrientation[1] = (float) (FILTER_COEFFICIENT
						* gyroOrientation[1] + oneMinusCoeff
						* (accMagOrientation[1] + 2.0 * Math.PI));
				fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else {
				fusedOrientation[1] = FILTER_COEFFICIENT * gyroOrientation[1]
						+ oneMinusCoeff * accMagOrientation[1];
			}

			// roll
			if (gyroOrientation[2] < -0.5 * Math.PI
					&& accMagOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTER_COEFFICIENT
						* (gyroOrientation[2] + 2.0 * Math.PI) + oneMinusCoeff
						* accMagOrientation[2]);
				fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else if (accMagOrientation[2] < -0.5 * Math.PI
					&& gyroOrientation[2] > 0.0) {
				fusedOrientation[2] = (float) (FILTER_COEFFICIENT
						* gyroOrientation[2] + oneMinusCoeff
						* (accMagOrientation[2] + 2.0 * Math.PI));
				fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI
						: 0;
			} else {
				fusedOrientation[2] = FILTER_COEFFICIENT * gyroOrientation[2]
						+ oneMinusCoeff * accMagOrientation[2];
			}

			// overwrite gyro matrix and orientation with fused orientation
			// to comensate gyro drift
			gyroMatrix = getRotationMatrixFromOrientation(fusedOrientation);
			System.arraycopy(fusedOrientation, 0, gyroOrientation, 0, 3);
			float angle = (float) (fusedOrientation[0] * 180 / Math.PI);

			if (angle < 0)
				angle = angle + 360;

			if (count > 1) {
				if (Math.abs(result_1[count - 2] - result_1[count - 1]) > 300) {
					result_1 = new float[10];
					count = 2;
					result_1[count - 1] = angle;
					result_1[count - 2] = angle;
				} else
					result_1[count - 1] = (float) (angle * 0.1 + result_1[count - 2] * 0.9);
			} else
				result_1[count - 1] = angle;

			float sum = 0;
			float avg = 0;

			for (int i = 0; i < count; i++) {
				sum = (float) (sum + result_1[i]);
				avg = (float) sum / count;
			}
			avg = (float) (avg * 0.2 + angle * 0.8);
			FusedMain.direction_value = avg;

			Message msg2 = mHandler.obtainMessage();
			msg2.obj = avg;
			msg2.what = 1;
			mHandler.sendMessage(msg2);
			count = count % 10;
			// update sensor output in GUI
			// mHandler.post(updateOreintationDisplayTask);
		}
	}

	public static void recalibrate() {
		angle[0] = 0.0f;
		angle[2] = 0.0f;
	}

	// **************************** GUI FUNCTIONS
	// *********************************

	public void updateOreintationDisplay() {
		Log.v("test", d.format(fusedOrientation[0] * 180 / Math.PI) + "****");

		/*
		 * switch(radioSelection) { case 0:
		 * mAzimuthView.setText(d.format(accMagOrientation[0] * 180/Math.PI) +
		 * '�'); mPitchView.setText(d.format(accMagOrientation[1] * 180/Math.PI)
		 * + '�'); mRollView.setText(d.format(accMagOrientation[2] *
		 * 180/Math.PI) + '�'); break; case 1:
		 * mAzimuthView.setText(d.format(gyroOrientation[0] * 180/Math.PI) +
		 * '�'); mPitchView.setText(d.format(gyroOrientation[1] * 180/Math.PI) +
		 * '�'); mRollView.setText(d.format(gyroOrientation[2] * 180/Math.PI) +
		 * '�'); break; case 2:
		 * mAzimuthView.setText(d.format(fusedOrientation[0] * 180/Math.PI) +
		 * '�'); mPitchView.setText(d.format(fusedOrientation[1] * 180/Math.PI)
		 * + '�'); mRollView.setText(d.format(fusedOrientation[2] * 180/Math.PI)
		 * + '�'); break; }
		 */
	}

	private Runnable updateOreintationDisplayTask = new Runnable() {
		public void run() {
			updateOreintationDisplay();
		}
	};
}