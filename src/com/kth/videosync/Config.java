package com.kth.videosync;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class Config {
	public static String key = "AIzaSyC9vEUBvhI0ZBWWi81TyvgMgzH0x5c1Zxk";
	public static String GoogApiURL = "https://maps.googleapis.com/maps/api/place/search/json";
	public static String radius = "radius=50";

	public static LatLng getProjLatLong(int radius, Location loc,
			float direction_value) {

		return new LatLng(
				loc.getLatitude()
						+ (180 * radius * Math.cos(direction_value
								* (Math.PI / 180))) / (Math.PI * 6378137),
				loc.getLongitude()
						+ (180 * radius
								* Math.sin(direction_value * (Math.PI / 180)) / (Math.PI * 6378137 * Math
								.cos(loc.getLatitude() * (Math.PI / 180)))));

	}
}
