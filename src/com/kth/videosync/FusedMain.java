package com.kth.videosync;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.radiusnetworks.ibeaconreference2.R;
import com.radiusnetworks.position.RangingService;

public class FusedMain extends Activity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
		OnMarkerDragListener, OnMapClickListener, OnSeekBarChangeListener,
		android.widget.CompoundButton.OnCheckedChangeListener,OnClickListener {

	private static LocationClient locationclient;
	private static SensorManager mSensorManager;
	static SensorFusionActivity sf;
	protected GoogleMap map;
	private static String TAG = "FusedMain";
	static TextView address;
	public static float direction_value;
	private LocationRequest locationrequest;
	private static Location location;
	private int radius = 5;
	private RadioGroup radius_group;
	static public Handler mHandler;
	private Polyline line;
	private Circle circle;
	private static int zoomlevel = 18;
	private SeekBar radiusy;
	private static int cirrad = 1;
	LinearLayout ll;
	CheckBox cb;
	public static boolean google = true;
	private TextView mts;
	private long start; 
	private int bar_progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		cb = (CheckBox) findViewById(R.id.checkBox1);
		cb.setOnCheckedChangeListener(this);
		mts = (TextView) findViewById(R.id.meters);
		radiusy = (SeekBar) findViewById(R.id.seekBar1);
		radiusy.setOnSeekBarChangeListener(this);
		radius_group = (RadioGroup) findViewById(R.id.maptype);
		radius_group.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				RadioButton rb = (RadioButton) findViewById(checkedId);
				switch (Integer.parseInt((String) rb.getText())) {
				case 1:
					map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					break;
				case 2:
					map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
					break;
				case 4:
					map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
					break;
				case 3:
					map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					break;
				}
			}
		});

		int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resp == ConnectionResult.SUCCESS) {
			locationclient = new LocationClient(this, this, this);
			locationclient.connect();
		} else {
			Toast.makeText(this, "Google Play Service Error " + resp,
					Toast.LENGTH_LONG).show();
		}

		getInit();

	}

	public void getInit() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sf = new SensorFusionActivity(mSensorManager, mHandler);
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		map.getUiSettings().setZoomControlsEnabled(false);
		map.animateCamera(CameraUpdateFactory.zoomTo(zoomlevel), 1000, null);

		// map.setOnMarkerDragListener(this);
		// map.setOnMapClickListener(this);
		address = (TextView) findViewById(R.id.address);
		ll = (LinearLayout) findViewById(R.id.businesslayout);
		mHandler = new Handler() {
			int count =0;
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (msg.what == 1) {
					count++;
					if (line != null)
						line.remove();
					if (location != null) {
						line = map.addPolyline(new PolylineOptions()
								.add(new LatLng(location.getLatitude(),
										location.getLongitude()),
										Config.getProjLatLong(radius, location,
												(Float) msg.obj)).width(5)
								.color(Color.RED));

						CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(),location.getLongitude()))
								.zoom(map.getCameraPosition().zoom)
								.bearing(direction_value).build();
						// Log.v("test", direction_value + "bearing");
						if(count%3==0){
							map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 1, null);
						}
					}
					if(count> 1000000) count = 0;
				}

				if (msg.what == 2) {
				}
				
				//iBeacon - entering Room
				if(msg.what == 3) {
					Log.v("lalala", "status: "+com.radiusnetworks.position.Config.if_4407);
					//TODO
				}
				
				//iBeacon - leaving Room
				if(msg.what == 4) {
					Log.v("lalala", "status: "+com.radiusnetworks.position.Config.if_4407);
					//TODO
				}

			}

		};
		startService(new Intent(FusedMain.this, RangingService.class));
	}

	@Override
	public void onLocationChanged(Location arg0) {
		location = arg0;
		map.clear();
		circle = map.addCircle(new CircleOptions()
				.center(new LatLng(arg0.getLatitude(), arg0.getLongitude()))
				.radius(cirrad).strokeColor(Color.RED).fillColor(Color.BLUE));
		LatLng NEWARK = new LatLng(59.405126, 17.949458);

		GroundOverlayOptions newarkMap = new GroundOverlayOptions()
		        .image(BitmapDescriptorFactory.fromResource(R.drawable.newark_nj_1922))
		        .position(NEWARK, 5f, 4f);

		// Add an overlay to the map, retaining a handle to the GroundOverlay object.
		GroundOverlay imageOverlay = map.addGroundOverlay(newarkMap);
		imageOverlay.setBearing(55);

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {

	}

	@Override
	public void onConnected(Bundle arg0) {
		location = locationclient.getLastLocation();
		if (locationclient != null && locationclient.isConnected()) {
			Log.v("test", "in the request");
			locationrequest = LocationRequest.create();
			locationrequest.setInterval(100);
			locationrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
			locationclient.requestLocationUpdates(locationrequest, this);
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
					location.getLatitude(), location.getLongitude()), zoomlevel));
		}
	}

	@Override
	public void onDisconnected() {

	}

	@Override
	protected void onPause() {
		sf.Stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		sf.Stop();
		sf = new SensorFusionActivity(mSensorManager, mHandler);
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		//added by kai, test...
		/*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setComponent(new ComponentName("com.radiusnetworks.position","com.radiusnetworks.position.MainActivity"));
        intent.putExtra("grace", "Hi");
        startActivity(intent);*/
		
		Log.v("test" , "stick touched");
		Location loc = location;
		map.clear();

		// double lat = loc.getLatitude()
		// + (180 * radius * Math.cos(direction_value * (Math.PI / 180)))
		// / (Math.PI * 6378137);
		// double longi = loc.getLongitude()
		// + (180 * radius * Math.sin(direction_value * (Math.PI / 180)) /
		// (Math.PI * 6378137 * Math
		// .cos(loc.getLatitude() * (Math.PI / 180))));
		LatLng latlng = Config
				.getProjLatLong(radius, location, direction_value);

		circle = map.addCircle(new CircleOptions()
				.center(new LatLng(loc.getLatitude(), loc.getLongitude()))
				.radius(cirrad).strokeColor(Color.RED).fillColor(Color.BLUE));

		new AddressPull(address).execute(latlng.latitude + ","
				+ latlng.longitude);

		// draggable

		/*
		 * map.addMarker(new MarkerOptions() .position(new LatLng(lat, longi))
		 * .icon
		 * (BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE
		 * )) .draggable(true));
		 */
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(
				new LatLng(loc.getLatitude(), loc.getLongitude()),
				map.getCameraPosition().zoom));
		Log.v("test", direction_value + "this is the direction");

		new DataPull(this, ll).execute(latlng.latitude + "", latlng.longitude
				+ "");

		// Intent intent = new Intent(this, DataPull.class);
		// intent.putExtra("latitude", lat+"");
		// intent.putExtra("longitude", longi+"");
		// startService(intent);

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (locationclient != null)
			locationclient.disconnect();
	}

	@Override
	public void onMarkerDrag(Marker arg0) {

	}

	@Override
	public void onMarkerDragEnd(Marker arg0) {
		Log.v("test", "marker dragged end");
		new AddressPull(address).execute(arg0.getPosition().latitude + ","
				+ arg0.getPosition().longitude);
	}

	@Override
	public void onMarkerDragStart(Marker arg0) {

	}

	@Override
	public void onMapClick(LatLng arg0) {
		location.setLatitude(arg0.latitude);
		location.setLongitude(arg0.longitude);
		Log.v("test", "clicked map and location changes");
		if (circle != null) {
			circle = map.addCircle(new CircleOptions()
					.center(new LatLng(location.getLatitude(), location
							.getLongitude())).radius(3).strokeColor(Color.RED)
					.fillColor(Color.BLUE));
		}

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		bar_progress = progress;
		radius = (int) ((((float) progress) / 100.00f) * 30);
		float level = (float) (((float) progress / 100.00f) * Math.pow(2,
				(1 - (map.getCameraPosition().zoom / map.getMaxZoomLevel()))
						* map.getMaxZoomLevel()));

		radius = (int) ((30 / 7) * level);
		mts.setText(radius+" mts");
		//Log.v("test", "radius dragged");
		// Log.v("test", "radius dragged" + (((float) progress) / 100.00f)*(
		// 64*Math.pow(2,map.getCameraPosition().zoom)));

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
//		start = System.currentTimeMillis();
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
/*		Log.v("test",System.currentTimeMillis()-start +"millis"+bar_progress);
		double zoomlevel =map.getCameraPosition().zoom ;
		if(System.currentTimeMillis()-start <200 && bar_progress == 100){
			
			//zoomlevel = ((int)map.getCameraPosition().zoom)-2;
			map.animateCamera(CameraUpdateFactory.zoomOut(), 1, null);
			Log.v("test",zoomlevel +"millisin end");
		}
		else if(System.currentTimeMillis()-start <200 && radius !=0  ){
			
			zoomlevel = 21 - Math.log(7*radius/30)/Math.log(2);
			Log.v("test",zoomlevel +"millis");
			if(zoomlevel> map.getMaxZoomLevel()) zoomlevel = map.getMaxZoomLevel();
			map.animateCamera(CameraUpdateFactory.zoomTo((float) zoomlevel), 1, null);
		}*/
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView.isChecked()) {
			google = false;
		} else {
			google = true;
		}
	}

}
