package com.kth.videosync;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.content.Context;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.radiusnetworks.ibeaconreference2.R;

public class MainActivity extends Activity {

	VideoView video_player_view;
	DisplayMetrics dm;
	SurfaceView sur_View;
	MediaController media_Controller;
	private static final int TWO_MINUTES = 1000 * 60 * 2;
	protected GoogleMap map;
	Marker hamburg;
	private static SensorManager mSensorManager;
	static SensorFusionActivity sf;
	static TextView address;
	public static String ADDRESS_URI = "https://maps.googleapis.com/maps/api/geocode/xml?sensor=true";
	protected static String TAG = "PlaceActivity";
	public static int direction_value;
	static final LatLng HAMBURG = new LatLng(53.558, 9.927);

	private LocationManager locationManager = null;
	private Location currentBestLocation = null;
	private LocationUpdateListener locationUpdateListener = null;
	public static final String LOGGER_TAG = "GeolocationSample";
	private static final int LOCATION_UPDATE_TIME_INTERVAL = 3 * 1000;
	private static final int LOCATION_UPDATE_DISTANCE_INTERVAL = 10;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		// setContentView(R.layout.activity_main);
		// locationManager = (LocationManager) this
		// .getSystemService(Context.LOCATION_SERVICE);

		// locationManager.requestLocationUpdates(
		// LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		getInit();

	}

	private class LocationUpdateListener implements LocationListener {
		public void onLocationChanged(Location location) {
			if (GeoLocationHelper.isBetterLocation(location,
					currentBestLocation)) {
				currentBestLocation = location;
				String status = "Lat: " + location.getLatitude() + " Long: "
						+ location.getLongitude();
				map.addMarker(new MarkerOptions().position(new LatLng(location
						.getLatitude(), location.getLongitude())));
				map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
						location.getLatitude(), location.getLongitude()), 15));
			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

	}

	private void stopLocationUpdates() {
		if (locationUpdateListener != null) {
			locationManager.removeUpdates(locationUpdateListener);
			locationUpdateListener = null;
		}
	}

	public void getInit() {
		// video_player_view = (VideoView) findViewById(R.id.video_player_view);
		// media_Controller = new MediaController(this);
		// dm = new DisplayMetrics();
		// this.getWindowManager().getDefaultDisplay().getMetrics(dm);
		// int height = dm.heightPixels;
		// int width = dm.widthPixels;
		// video_player_view.setMinimumWidth(width);
		// video_player_view.setMinimumHeight(height);
		// video_player_view.setMediaController(media_Controller);
		// video_player_view.setVideoPath("rtsp://130.237.238.111:8554/video.mp4");
		// video_player_view.start();
		startLocationUpdates();
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		// sf = new SensorFusionActivity(mSensorManager);
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();
		map.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);
	}

	@Override
	public void onBackPressed() {
		stopLocationUpdates();
		super.onBackPressed();
	}

	private void startLocationUpdates() {
		if (locationUpdateListener != null) {
			return;
		}
		if (gpsEnabled()) {
			String provider = GeoLocationHelper
					.getLocationProviderByPriority(getApplicationContext());
			Log.v("test", provider);
			locationUpdateListener = new LocationUpdateListener();
			locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER,
					LOCATION_UPDATE_TIME_INTERVAL,
					LOCATION_UPDATE_DISTANCE_INTERVAL, locationUpdateListener);
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER,
					LOCATION_UPDATE_TIME_INTERVAL,
					LOCATION_UPDATE_DISTANCE_INTERVAL, locationUpdateListener);

		} else {
			GeoLocationHelper.showGpsSettingsScreen(this);
		}
	}

	private boolean gpsEnabled() {
		String provider = GeoLocationHelper
				.getLocationProviderByPriority(getApplicationContext());
		Log.v(LOGGER_TAG, "provider: " + provider);
		if (provider == null) {
			return false;
		} else {
			return true;
		}
	}

	public void onClick1(View v) {
		// Location lastKnownLocation =
		// lastLocationFinder.getLastBestLocation(PlacesConstants.MAX_DISTANCE,
		// System.currentTimeMillis()-PlacesConstants.MAX_TIME);

		// Force an update
		// updatePlaces(lastKnownLocation, PlacesConstants.DEFAULT_RADIUS,
		// true);
		// map.clear();
		// double lat = lastKnownLocation.getLatitude() + (180*30*
		// Math.cos(direction_value*(Math.PI/180)))/(Math.PI*6378137);
		double lat = HAMBURG.latitude
				+ (180 * 30 * Math.cos(direction_value * (Math.PI / 180)))
				/ (Math.PI * 6378137);
		// double longi = lastKnownLocation.getLongitude() +
		// (180*30*Math.sin(direction_value*(Math.PI/180))/(Math.PI*6378137*Math.cos(lastKnownLocation.getLatitude()*(Math.PI/180))));
		double longi = HAMBURG.longitude
				+ (180 * 30 * Math.sin(direction_value * (Math.PI / 180)) / (Math.PI * 6378137 * Math
						.cos(HAMBURG.latitude * (Math.PI / 180))));
		getAddress(lat + "," + longi);
		map.addMarker(new MarkerOptions().position(new LatLng(lat, longi)));
		// map.moveCamera(CameraUpdateFactory.newLatLngZoom(new
		// LatLng(lastKnownLocation.getLatitude(),
		// lastKnownLocation.getLongitude()), 15));
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
				HAMBURG.latitude, HAMBURG.longitude), 15));
		Log.v("test", direction_value + "this is the direction");
	}

	public void onClick(View v) {
		if (currentBestLocation != null) {
			map.clear();
			double lat = currentBestLocation.getLatitude()
					+ (180 * 30 * Math.cos(direction_value * (Math.PI / 180)))
					/ (Math.PI * 6378137);
			double longi = currentBestLocation.getLongitude()
					+ (180 * 30 * Math.sin(direction_value * (Math.PI / 180)) / (Math.PI * 6378137 * Math
							.cos(currentBestLocation.getLatitude()
									* (Math.PI / 180))));
			getAddress(lat + "," + longi);
			map.addMarker(new MarkerOptions().position(new LatLng(lat, longi)));
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(currentBestLocation.getLatitude(),
							currentBestLocation.getLongitude()), 15));
			Log.v("test", direction_value + "this is the direction");
		}
	}

	public void getAddress(String latlong) {

		// long currentTime = System.currentTimeMillis();
		URL url;

		try {
			// TODO Replace this with a URI to your own service.
			// String locationStr = location.getLatitude() + "," +
			// location.getLongitude();
			String baseURI = ADDRESS_URI;
			String placesFeed = baseURI + "&latlng=" + latlong;
			url = new URL(placesFeed);

			// Open the connection
			URLConnection connection = url.openConnection();
			HttpsURLConnection httpConnection = (HttpsURLConnection) connection;
			int responseCode = httpConnection.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				// Use the XML Pull Parser to extract each nearby location.
				// TODO Replace the XML parsing to extract your own place list.
				InputStream in = httpConnection.getInputStream();

				XmlPullParserFactory factory = XmlPullParserFactory
						.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();
				boolean found = false;
				xpp.setInput(in, null);
				int eventType = xpp.getEventType();
				while (eventType != XmlPullParser.END_DOCUMENT) {
					if (eventType == XmlPullParser.START_TAG
							&& xpp.getName().equals("result") && found == false) {
						eventType = xpp.next();
						String formatted_address = "";
						while (!(eventType == XmlPullParser.END_TAG && xpp
								.getName().equals("result"))) {
							if (eventType == XmlPullParser.START_TAG
									&& xpp.getName()
											.equals("formatted_address")) {
								formatted_address = xpp.nextText();
								address = (TextView) findViewById(R.id.address);
								address.setText(formatted_address);
								// Log.v("test",formatted_address);
								found = true;
								break;
							}
							eventType = xpp.next();
						}
					}
					eventType = xpp.next();
				}
			} else
				Log.e(TAG,
						responseCode + ": "
								+ httpConnection.getResponseMessage());

		} catch (MalformedURLException e) {
			Log.e(TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		} catch (XmlPullParserException e) {
			Log.e(TAG, e.getMessage());
		}
	}

	@Override
	protected void onPause() {
		sf.Stop();
		super.onPause();
	}

	@Override
	protected void onResume() {
		sf.Stop();
		// sf = new SensorFusionActivity(mSensorManager);
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
