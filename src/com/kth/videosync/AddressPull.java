package com.kth.videosync;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.protocol.HTTP;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

public class AddressPull extends AsyncTask<String, Void, String> {

	TextView address;
	public static String ADDRESS_URI = "https://maps.googleapis.com/maps/api/geocode/xml?sensor=true";
	public static String ADDRESS_URI_JSON = "https://maps.googleapis.com/maps/api/geocode/json?sensor=true";
	private static String TAG = "AddressPuller";
	private static String mAddress;
	public static String street_num;

	public AddressPull(TextView add) {
		super();
		this.address = add;
	}

	@Override
	protected String doInBackground(String... arg0) {
		Log.v("test", "inbackground");
		getAddressJSON(arg0[0]);
		// return getAddress(arg0[0]);
		return null;
	}

	protected void onPostExecute(String result) {
		Log.v("test", "setting text" + street_num);
		address.setText(mAddress);

	}

	public static void getAddressJSON(String latlong) {
		String resp;
		try {

			String baseURI = ADDRESS_URI_JSON;
			String placesFeed = baseURI + "&latlng=" + latlong;
			URL obj = new URL(placesFeed);
			// url = new URL(placesFeed);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			// int responseCode = con.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream(), HTTP.UTF_8));
			String inputLine;
			StringBuffer response = new StringBuffer();

			StringBuilder builder = new StringBuilder();
			while ((inputLine = in.readLine()) != null) {
				// response.append(inputLine);
				builder.append(inputLine);
			}
			in.close();

			JSONTokener tokener = new JSONTokener(builder.toString());

			resp = response.toString();
			JSONObject object = new JSONObject(tokener);
			JSONObject address_comp = object.getJSONArray("results")
					.getJSONObject(0);

			mAddress = address_comp.getString("formatted_address");
			street_num = address_comp.getJSONArray("address_components")
					.getJSONObject(0).getString("long_name");
			street_num.replaceAll("\\s+", "");

		} catch (Exception ex) {
			ex.printStackTrace();
			resp = null;
		}
	}

	public static String getAddress(String latlong) {

		// long currentTime = System.currentTimeMillis();
		URL url;
		String formatted_address = "";
		try {
			// TODO Replace this with a URI to your own service.
			// String locationStr = location.getLatitude() + "," +
			// location.getLongitude();
			String baseURI = ADDRESS_URI;
			String placesFeed = baseURI + "&latlng=" + latlong;
			url = new URL(placesFeed);

			// Open the connection
			URLConnection connection = url.openConnection();
			HttpsURLConnection httpConnection = (HttpsURLConnection) connection;
			int responseCode = httpConnection.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				// Use the XML Pull Parser to extract each nearby location.
				// TODO Replace the XML parsing to extract your own place list.
				InputStream in = httpConnection.getInputStream();

				XmlPullParserFactory factory = XmlPullParserFactory
						.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();
				boolean found = false;
				xpp.setInput(in, null);
				int eventType = xpp.getEventType();
				while (eventType != XmlPullParser.END_DOCUMENT) {
					if (eventType == XmlPullParser.START_TAG
							&& xpp.getName().equals("result") && found == false) {
						eventType = xpp.next();

						while (!(eventType == XmlPullParser.END_TAG && xpp
								.getName().equals("result"))) {
							if (eventType == XmlPullParser.START_TAG
									&& xpp.getName()
											.equals("formatted_address")) {
								formatted_address = xpp.nextText();
								// address = (TextView)
								// findViewById(R.id.address);
								// address.setText(formatted_address);
								// Log.v("test",formatted_address);
								found = true;
								break;
							}
							eventType = xpp.next();
						}
					}
					eventType = xpp.next();
				}
			} else
				Log.e(TAG,
						responseCode + ": "
								+ httpConnection.getResponseMessage());

		} catch (MalformedURLException e) {
			Log.e(TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		} catch (XmlPullParserException e) {
			Log.e(TAG, e.getMessage());
		}
		return formatted_address;
	}

}
