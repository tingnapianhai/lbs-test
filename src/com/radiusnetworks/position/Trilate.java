package com.radiusnetworks.position;

import android.util.Log;

public class Trilate {
	public static int test = 0;
	
	//self
	public double[] calcu(double d, double[] P3, double r1, double r2, double r3) {
		double[] point = { 0, 0 };

		point[0] = (Math.pow(r1, 2) - Math.pow(r2, 2) + Math.pow(d, 2))	/ (2 * d);

		point[1] = (Math.pow(r1, 2) - Math.pow(r3, 2) + Math.pow(P3[0], 2) + Math
				.pow(P3[1], 2)) / (2 * P3[1]) - P3[0] * point[0] / P3[1];

		return point;
	}
	
	//from CSDN
	public double[] trilateration(double x1, double y1, double d1,
			double x2, double y2, double d2, double x3, double y3, double d3) {
		double[] d = { 0.0, 0.0 };
		double a11 = 2 * (x1 - x3);
		double a12 = 2 * (y1 - y3);
		double b1 = Math.pow(x1, 2) - Math.pow(x3, 2) + Math.pow(y1, 2)	- Math.pow(y3, 2) + Math.pow(d3, 2) - Math.pow(d1, 2);
		double a21 = 2 * (x2 - x3);
		double a22 = 2 * (y2 - y3);
		double b2 = Math.pow(x2, 2) - Math.pow(x3, 2) + Math.pow(y2, 2)	- Math.pow(y3, 2) + Math.pow(d3, 2) - Math.pow(d2, 2);

		d[0] = (b1 * a22 - a12 * b2) / (a11 * a22 - a12 * a21);
		d[1] = (a11 * b2 - b1 * a21) / (a11 * a22 - a12 * a21);

		return d;
	}
	
	//just the test-sample, unit test ?
	public void instaitate () {
		double[] P3 = { 0, 1 };
		
		double[] aa = calcu(1, P3, 2, 1, Math.pow(5, 0.5));
		Log.v("Trilate","" + aa[0] + " + " + aa[1]);
		
		double[] bb = trilateration(0,0,2, 1,0,1, 0,1,Math.pow(5, 0.5));
		Log.v("Trilate","" + bb[0] + " + " + bb[1]);
	}
	
	
	
}