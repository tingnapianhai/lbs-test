package com.radiusnetworks.position;

import java.util.Collection;

import com.kth.videosync.FusedMain;
import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.Region;
import com.radiusnetworks.ibeacon.RangeNotifier;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.app.Service;
import android.content.Intent;

public class CopyOfRangingService extends Service implements IBeaconConsumer {
	protected final String TAG = "APRangingService";
	private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);
	private Handler handler;
	private Message msg;
	private final long timeInterval = 6;
	
	private static long time1 = System.currentTimeMillis();
	private static long time2 = System.currentTimeMillis();

	public CopyOfRangingService() {
		super();
		this.handler = FusedMain.mHandler;
		com.radiusnetworks.position.Config.timeLastInRoom = System.currentTimeMillis();
		Log.v(TAG, "rangingservice Constructor");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		iBeaconManager.bind(this);
		Log.v(TAG, "ranging service onCreate started");
	}

	@Override
	public void onIBeaconServiceConnect() {
		iBeaconManager.setRangeNotifier(new RangeNotifier() {

			@Override
			public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {

				Log.v(TAG, "@@@@@@@@@@@@@@@ ");
				time2 = System.currentTimeMillis();
				String dif_time = "" + (time2 - time1);
				time1 = time2;

				//seems works, wahaha, hahahaaaa
				if (iBeacons.size() > 0) {
					// iBeacons.iterator().next().getProximityUuid();
					for (IBeacon ibea : iBeacons) {
						
						// ! verify/check MSL-iBeacons;
						if(!ibea.getProximityUuid().equalsIgnoreCase(Config.iBeacon_UUID) || ibea.getMinor()!=88 ) {
							Log.v(TAG, "non-MSL iBeacon exists " + dif_time);
							Config.rssi_4407[0]=-100;
							continue;
						}

						String info = dif_time + " " + iBeacons.size() + " Minor:" + ibea.getMinor() + " Rssi:"	+ ibea.getRssi();
						saveIBeaconRssi(ibea.getMinor(), ibea.getRssi());
						
						/*msg = new Message();
						msg.what = 0;
						msg.obj = ibea.getMinor()-86;
						handler.sendMessage(msg);*/
						
						Log.v(TAG, ibea.getProximityUuid()+"--- " + info);
						Log.v(TAG, "--"+Config.if_4407_rangeSignal+"--");
					}//end for
					/*msg = new Message();
					msg.what = 1;
					handler.sendMessage(msg);*/
				}//end if
				else {
					Config.rssi_4407[0]=-100;
					Log.v(TAG, "none--" + dif_time);
				}
				checkIBeaconRoom();
				notifyIBeaconRoomStatus();
				Log.v("lalala", ""+Config.if_4407_rangeSignal_pre+Config.if_4407_rangeSignal+Config.if_4407_pre+Config.if_4407);
			}
		});

		try {
			iBeaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
			} 
		catch (RemoteException e) {	}
	}
	
	private void saveIBeaconRssi(int minor, int rssi) {
		//verify if this ibeacon is msl-ibeacon;
		if(minor<88 || minor>89)
			return;
		int num = minor-88;
		Config.rssi_4407[num] = rssi;
	}
	private void checkIBeaconRoom() {
		Config.if_4407_pre = Config.if_4407;
		Config.if_4407_rangeSignal_pre = Config.if_4407_rangeSignal;
		
		if(Config.rssi_4407[0]>-80) {
			Config.if_4407_rangeSignal = true;
			Config.timeLastInRoom = System.currentTimeMillis();//record the last-time inside the ibeacon range;
		}
		else
			Config.if_4407_rangeSignal = false;
	}
	private void notifyIBeaconRoomStatus() {
		
		long time = System.currentTimeMillis();
		if(Config.if_4407_rangeSignal) {
			Config.if_4407 = true;
		}
		else if(!Config.if_4407_rangeSignal) {
			if( (time-Config.timeLastInRoom)/1000 >= timeInterval ) {
				Config.if_4407 = false;
			}
		}
		
		msg = new Message();
		//entering...
		if(!Config.if_4407_pre && Config.if_4407) {
			msg.what = 3;
			msg.obj = Config.if_4407;
			handler.sendMessage(msg);
		}
		//leaving...
		else if(Config.if_4407_pre && !Config.if_4407) {
			msg.what = 4;
			msg.obj = Config.if_4407;
			handler.sendMessage(msg);
		}		
	}
	
	/*//往array里面插入元素
	private void renewRssiSamples(int[] arr, int rssi) {
    	for(int i=arr.length-1;i>0;i--) {
    		arr[i] = arr[i-1];
    	}
    	arr[0] = rssi;
    }*/

	private void logToDisplay(final String line) {
		new Thread(new Runnable() {
			public void run() {
				// TODO
				String str = line + "\n";
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		iBeaconManager.unBind(this);
		Log.v(TAG, "ranging service onDestroy :(((((");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
}
